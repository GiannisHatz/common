import {TestBed, async, inject} from '@angular/core/testing';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {UserStorageService} from './user-storage';
import {AngularDataContext, MostModule} from '@themost/angular';
import {ApiTestingController} from '../../../testing/src/api-testing-controller.service';
import {ApiTestingModule} from '../../../testing/src/api-testing.module';


describe('UserStorageService', () => {

    let mockApi: ApiTestingController;
    beforeEach(async(() => {
        // noinspection JSIgnoredPromiseFromCall
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                FormsModule,
                MostModule.forRoot({
                    base: '/api/',
                    options: {
                        useMediaTypeExtensions: false
                    }
                }),
                ApiTestingModule.forRoot()
            ],
            providers: [
                UserStorageService
            ],
            declarations: [
            ],
        }).compileComponents();
        mockApi = TestBed.get(ApiTestingController);
    }));

    it('should create instance', inject([AngularDataContext], (context: AngularDataContext) => {
        const userStorageService = new UserStorageService(context);
        expect(userStorageService).toBeTruthy();
    }));

    it('should have user storage', inject([UserStorageService], async (userStorage: UserStorageService) => {
        mockApi.match({
            url: '/api/diagnostics/services',
            method: 'GET'
        }).map(request => {
           request.flush([
               {
                   serviceType: 'UserStorageService'
               }
           ]);
        });
        const hasStorage = await userStorage.hasUserStorage();
        expect(hasStorage).toBeTruthy();

    }));

    it('should not have user storage', inject([UserStorageService], async (userStorage: UserStorageService) => {
        mockApi.match({
            url: '/api/diagnostics/services',
            method: 'GET'
        }).map(request => {
            request.flush([
            ]);
        });
        const hasStorage = await userStorage.hasUserStorage();
        expect(hasStorage).toBeFalsy();
    }));

    it('should get user storage item', inject([UserStorageService], async (userStorage: UserStorageService) => {
        mockApi.match({
            url: '/api/diagnostics/services',
            method: 'GET'
        }).map(request => {
            request.flush([
                {
                    serviceType: 'UserStorageService'
                }
            ]);
        });
        mockApi.match({
            url: '/api/users/me/storage/get',
            method: 'POST'
        }).map(request => {
            request.flush({
                key: 'registrar/lastDepartment',
                value: null
            });
        });
        const result = await userStorage.getItem('registrar/department');
        expect(result).toBeTruthy();
        expect(result.value).toBeFalsy();
    }));

    it('should throw forbidden', inject([UserStorageService], async (userStorage: UserStorageService) => {
        mockApi.match({
            url: '/api/diagnostics/services',
            method: 'GET'
        }).map(request => {
            request.flush([
                {
                    serviceType: 'UserStorageService'
                }
            ]);
        });
        mockApi.match({
            url: '/api/users/me/storage/get',
            method: 'POST'
        }).map(request => {
            request.error(new ErrorEvent('Forbidden'), {
                status: 403,
                statusText: 'Forbidden'
            });
        });
        userStorage.getItem('registrar/department').then( () => {
            expect(false).toBeTruthy('Expected error to be thrown');
        }).catch( err => {
            expect(err).toBeTruthy();
        });
    }));

});
