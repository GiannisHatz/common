import { ErrorHandler, ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import {
    ErrorBaseComponent,
    HttpErrorComponent
} from './components/error-base/error-base.component';
import { ErrorRoutingModule } from './error.routing';
import { ErrorService, ErrorModalComponent } from './error.service';
import { ErrorsHandler } from './error.handler';
import { environment } from '../environments/environment';

import {ERROR_LOCALES} from './i18n';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ErrorRoutingModule
    ],
    providers: [

    ],
    declarations: [
        ErrorBaseComponent,
        HttpErrorComponent,
        ErrorModalComponent
    ],
    exports: [
        ErrorBaseComponent,
        HttpErrorComponent
    ],
    entryComponents: [
        ErrorModalComponent
    ]
})
export class ErrorModule {

    constructor( @Optional() @SkipSelf() parentModule: ErrorModule, private _translateService: TranslateService) {
        this.ngOnInit();
    }

    static forRoot(): ModuleWithProviders<ErrorModule> {
        return {
            ngModule: ErrorModule,
            providers: [
                ErrorService,
                {
                    provide: ErrorHandler,
                    useClass: ErrorsHandler,
                }
            ]
        };
    }

    // tslint:disable-next-line:use-life-cycle-interface
    ngOnInit() {
        environment.languages.forEach( language => {
            if (ERROR_LOCALES.hasOwnProperty(language)) {
                this._translateService.setTranslation(language, ERROR_LOCALES[language], true);
            }
        });
      }
}
