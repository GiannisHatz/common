import { SHA1 } from 'crypto-js';
/**
 * Returns a string which represents a key generated by @asyncMemoize() or @memoize() decorators
 * @param target - The target object
 * @param propertyKey - A string which represents the method that is going to be called
 * @param args - An optional param array of arguments
 * @example
 * // calculate memoized key for myObject.getFunc1(100, true)
 * const key = getMemoizeKey(myObject, 'getFunc1', 100, true);
 */
export function getMemoizeKey(target: any, propertyKey: string, ...args: any[]) {
    let computedHash;
    // get arguments hash
    if (args.length) {
        computedHash = SHA1(JSON.stringify(args));
    }
    let targetConstructor = target.constructor.name;
    // validate static function
    if (targetConstructor === 'Function') {
        targetConstructor = target.prototype.constructor.name;
    }
    if (computedHash) {
        // add computed hash to key
        return`${targetConstructor}.${propertyKey}.${computedHash}`;
    } else {
        return `${targetConstructor}.${propertyKey}`;
    }
}

/**
 * Removes a key-value pair generated by @asyncMemoize() or @memoize() decorators
 * @param target - The target object
 * @param propertyKey - A string which represents the method that is going to be called
 * @param args - An optional param array of arguments
 * @example
 * // removes memoized key for myObject.getFunc1(100, true)
 * removeMemoizeKey(myObject, 'getFunc1', 100, true);
 */
export function removeMemoizeKey(target: any, propertyKey: string, ...args: any[]) {
    // get key
    const key = getMemoizeKey.apply(null, arguments);
    // remove item from storage
    sessionStorage.removeItem(key);
}

/**
 * Use @asyncMemoize() decorator to memoize the result of an async method to storage
 * @example
 * class TestClass1 {
 *     @asyncMemoize()
 *     async getItems() {
 *         return ['apple', 'lemon', 'orange'];
 *     }
 * }
 */
export function asyncMemoize() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        // validate that descriptor.value is a function
        if (typeof descriptor.value !== 'function') {
            throw new Error('Invalid decorator descriptor. @memoize() decorator should be applied in class methods.');
        }
        // get original descriptor value (which is a method)
        const func = descriptor.value;
        // change descriptor
        // important: use standard function to avoid error
        // The 'arguments' object cannot be referenced in an arrow function in ES3 and ES5.
        descriptor.value = function() {
            const args = Array.from(arguments);
            const thisArg = this;
            return new Promise(function(resolve, reject) {
                const key = getMemoizeKey.apply(null, [ target, propertyKey ].concat(args));
                // try to get item from storage
                const valueString = sessionStorage.getItem(key);
                // parse value string
                if (valueString) {
                    try {
                        return resolve(JSON.parse(valueString));
                    } catch (err) {
                        return reject(err);
                    }
                }
                // call method
                return func.apply(thisArg, args).then(function(value) {
                    // set item to storage
                    sessionStorage.setItem(key, JSON.stringify(value));
                    // and finally return result
                    return resolve(value);
                }).catch(reason => {
                    return reject(reason);
                });
            });

        };
    };
}

/**
 * Use @memoize() decorator to memoize the result of an async method to storage
 * @example
 * class TestClass1 {
 *     @memoize()
 *     getItems() {
 *         return ['apple', 'lemon', 'orange'];
 *     }
 * }
 */
export function memoize() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        // validate that descriptor.value is a function
        if (typeof descriptor.value !== 'function') {
            throw new Error('Invalid decorator descriptor. @memoize() decorator should be applied in class methods.');
        }
        // get original descriptor value (which is a method)
        const func = descriptor.value;
        // change descriptor
        descriptor.value = function() {
            const key = getMemoizeKey.apply(null, [ target, propertyKey ].concat(Array.from(arguments)));
            // try to get item from storage
            const valueString = sessionStorage.getItem(key);
            if (valueString) {
                return JSON.parse(valueString);
            }
            // call method
            const value = func.apply(this, arguments);
            // set item to session storage
            sessionStorage.setItem(key, JSON.stringify(value));
            // and finally return result
            return value;
        };
    };
}
