import {Component, EventEmitter, Input} from '@angular/core';
import { RouterModal } from './RouterModal';
import { ButtonTypes, ButtonType } from './ButtonTypes';
import { RouterModalYesNo } from './RouterModalYesNo';
// noinspection JSUnusedGlobalSymbols

export abstract class RouterModalPreviousNextCancel extends RouterModal {

    public readonly buttonChanges = new EventEmitter<{ [ button: string]: ButtonType }>();

    protected get previousButtonState(): { previous: ButtonType } {
        return {
            previous: {
                buttonText: this._previousButtonText,
                buttonClass: this._previousButtonClass,
                buttonDisabled: this._previousButtonDisabled
            }
        };
    }

    protected get nextButtonState(): { next: ButtonType } {
        return {
            next: {
                buttonText: this._nextButtonText,
                buttonClass: this._nextButtonClass,
                buttonDisabled: this._nextButtonDisabled
            }
        };
    }

    protected get cancelButtonState(): { cancel: ButtonType } {
        return {
            cancel: {
                buttonText: this._cancelButtonText,
                buttonClass: this._cancelButtonClass,
                buttonDisabled: this._cancelButtonDisabled
            }
        };
    }

    private _previousButtonText = ButtonTypes.previous.buttonText;
    @Input()
    public get previousButtonText() {
        return this._previousButtonText;
    }
    public set previousButtonText(value) {
        this._previousButtonText = value;
        this.buttonChanges.emit(this.previousButtonState);
    }

    private _previousButtonClass = ButtonTypes.previous.buttonClass;
    @Input()
    public get previousButtonClass() {
        return this._previousButtonClass;
    }
    public set previousButtonClass(value) {
        this._previousButtonClass = value;
        this.buttonChanges.emit(this.previousButtonState);
    }

    private _previousButtonDisabled = ButtonTypes.previous.buttonDisabled;
    @Input()
    public get previousButtonDisabled() {
        return this._previousButtonDisabled;
    }
    public set previousButtonDisabled(value) {
        this._previousButtonDisabled = value;
        this.buttonChanges.emit(this.previousButtonState);
    }

    private _nextButtonText = ButtonTypes.next.buttonText;
    @Input()
    public get nextButtonText() {
        return this._nextButtonText;
    }
    public set nextButtonText(value) {
        this._nextButtonText = value;
        this.buttonChanges.emit(this.nextButtonState);
    }

    private _nextButtonClass = ButtonTypes.next.buttonClass;
    @Input()
    public get nextButtonClass() {
        return this._nextButtonClass;
    }
    public set nextButtonClass(value) {
        this._nextButtonClass = value;
        this.buttonChanges.emit(this.nextButtonState);
    }

    private _nextButtonDisabled = ButtonTypes.next.buttonDisabled;
    @Input()
    public get nextButtonDisabled() {
        return this._nextButtonDisabled;
    }
    public set nextButtonDisabled(value) {
        this._nextButtonDisabled = value;
        this.buttonChanges.emit(this.nextButtonState);
    }

    private _cancelButtonText = ButtonTypes.cancel.buttonText;
    @Input()
    public get cancelButtonText() {
        return this._cancelButtonText;
    }
    public set cancelButtonText(value) {
        this._cancelButtonText = value;
        this.buttonChanges.emit(this.cancelButtonState);
    }

    private _cancelButtonClass = ButtonTypes.cancel.buttonClass;
    @Input()
    public get cancelButtonClass() {
        return this._cancelButtonClass;
    }
    public set cancelButtonClass(value) {
        this._cancelButtonClass = value;
        this.buttonChanges.emit(this.cancelButtonState);
    }

    private _cancelButtonDisabled = ButtonTypes.cancel.buttonDisabled;
    @Input()
    public get cancelButtonDisabled() {
        return this._cancelButtonDisabled;
    }
    public set cancelButtonDisabled(value) {
        this._cancelButtonDisabled = value;
        this.buttonChanges.emit(this.cancelButtonState);
    }
    abstract previous(): Promise<any>;
    abstract next(): Promise<any>;
    abstract cancel(): Promise<any>;
}
