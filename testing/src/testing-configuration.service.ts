
export class TestingConfigurationService {
    public config: any;
    private currentLang: string;
    constructor() {
        this.config = {
            settings: {
                localization: {
                    cultures: [ 'en', 'el' ],
                    default: 'el'
                }
            }
        };
        this.currentLang = this.config.settings.localization.default;
    }

    get settings(): any {
        return this.config.settings;
    }

    getCurrentLang(): string {
        return this.currentLang;
    }

    setCurrentLang(lang: string) {
        this.currentLang = lang;
    }
}
